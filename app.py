from flask import Flask, render_template, request, redirect, url_for, Response
from flask_bootstrap import Bootstrap
import boto3
from config import S3_BUCKET, S3_KEY, S3_SECRET
import os
from filters import datetimeformat
import json
from itertools import groupby

s3_resource = boto3.resource(
   "s3",
   aws_access_key_id=S3_KEY,
   aws_secret_access_key=S3_SECRET
)

app = Flask(__name__)
Bootstrap(app)
app.jinja_env.filters['datetimeformat'] = datetimeformat

@app.route('/')
def index():
   return render_template("index.html")

@app.route('/files')
def files():
    bucket = 'mercury-website-config'
    #Make sure you provide / in the end
    prefix = 'squad_intro/squad_intro.txt'  
    #prefix = 'squad_intro' 

    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket(bucket)

    # for object_summary in my_bucket.objects.filter(Prefix=prefix):
    #     print(object_summary.key)
    s3_resource = boto3.resource('s3')
    #my_bucket = s3_resource.Bucket("mercury-website-config")

    my_bucket = s3_resource.Bucket(name="mercury-website-config")
    #startAfter = 'squad_intro' 
    summaries = my_bucket.objects.all()
    #summaries = my_bucket.objects.filter(Prefix=prefix)
    #summaries = s3_resource.list_objects_v2(Bucket=my_bucket, StartAfter=startAfter) 

    return render_template('files.html', my_bucket=my_bucket, files=summaries)

@app.route('/upload', methods=['POST'])
def upload():
    #prefix = 'squad_intro/'
    file = request.files['file']
    #print(file)
    
    #s3_resource = boto3.resource('s3')
    #my_bucket = s3_resource.Bucket("mercury-website-config")
    #my_bucket.Object(file.filename).put(Body=file)

    s3 = boto3.client('s3')
    s3.upload_file(Bucket="mercury-website-config", Key="images/"+file.filename, Filename="/Users/zarrarmalik/downloads/"+file.filename)

    return "uploaded"


@app.route('/delete', methods=['POST'])
def delete():
    key = request.form['key']

    s3_resource = boto3.resource('s3')
    my_bucket = s3_resource.Bucket("mercury-website-config")
    my_bucket.Object(key).delete()
    return redirect(url_for('files'))

@app.route('/download', methods=['POST'])
def download():
    key = request.form['key']

    s3_resource = boto3.resource('s3')
    my_bucket = s3_resource.Bucket("mercury-website-config")

    file_obj = my_bucket.Object(key).get()
    readFileFromS3()
    convertToJson()
    return Response(
        file_obj['Body'].read(),
        mimetype='text/plain',
        headers={"Content-Disposition": "attachment;filename={}".format(key)}
    )

def readFileFromS3():
    s3client = boto3.client(
    's3'
    )
    # These define the bucket and object to read
    bucketname = "mercury-website-config" 
    file_to_read = "squad_intro/squad_intro.txt"

    #Create a file object using the bucket and object key. 
    fileobj = s3client.get_object(
    Bucket=bucketname,
    Key=file_to_read
    ) 
    # open the file object and read it into the variable filedata. 
    filedata = fileobj['Body'].read()

    # file data will be a binary stream.  We have to decode it 
    contents = filedata.decode('utf-8') 

    # Once decoded, you can treat the file as plain text if appropriate 
    print(contents)

def convertToJson():
    # person = '{"name": "Bob", "languages": ["English", "Fench"]}'
    # person_dict = json.loads(person)

    # # Output: {'name': 'Bob', 'languages': ['English', 'Fench']}
    # print( person_dict)

    # # Output: ['English', 'French']
    # print(person_dict['languages'])
    filename = "kilburn_squad.txt"

    # dictionary where the lines from 
    # text will be stored 
    #dict1 = {} 
    #dataList=[]
    #listWithNoSpaces=[]
    #finalListWithSkippedLines=[]

    # creating dictionary 
    #with open(filename) as fh: 
        # dataList=fh.readlines()
        # print(dataList)
        # for x in dataList:
        #     print(x.strip())
        #     val=x.strip()
        #     listWithNoSpaces.append(val)
        # finalListWithSkippedLines=list(filter(''.__ne__,listWithNoSpaces))
        # print(listWithNoSpaces)
        # print(finalListWithSkippedLines)

        #for line in fh: 
  
        # reads each line and trims of extra the spaces  
        # and gives only the valid words 
            ##command, description = line.strip().split(None, 1) 
  
           # dict1[command] = description.strip() 
  
    # creating json file 
    # the JSON file is named as test1 
    #out_file = open("test1.json", "w") 
    #json.dump(dict1, out_file, indent = 4, sort_keys = False) 
    #out_file.close() 
    names = ["name", "lastName", "occupation", "description"]
    with open(filename, 'r') as f, open('test1.json', 'w') as out:
        grouped = groupby(map(str.rstrip,f), key=lambda x: x.startswith("|-"))
        for k,v in grouped:
            if not k:
                json.dump(dict(zip(names,v)),out)
                out.write("\n")

if __name__=='__main__':
    app.run()